import dis
from types import CodeType
import typing

#def checker_func(val, type_, funcname, filepath, lineno):
checker_func = """
if not isinstance({val}, {type_}):
    err = "Bad type for {funcname}: Expected {type_} got {realtype}"
    raise TypeError(err)
"""


def gen_bytecode(frame, func):
    filepath = frame.f_code.co_filename
    funcname = frame.f_code.co_name
    lineno = frame.f_code.co_firstlineno
    old_code = frame.f_code

    new_code = b''

    for (name, type_) in func.__annotations__.items():
        try:
            val = frame.f_locals[name]
        except KeyError:
            try:
                val = frame.f_globals[name]
            except KeyError:
                return old_code

        # need to detect if literal
        compiled = compile(checker_func.format(val=repr(val), type_=type_.__name__, funcname=funcname, realtype=type(val)), filepath, "exec")
        code = dis.Bytecode(compiled, first_line=lineno)
        new_code += code.codeobj.co_code[:-4]
        print(new_code)
        print(new_code + old_code.co_code)
        print(dis.dis(new_code + old_code.co_code))
        # we need to load isinstance, and the types into the symbol table

    return CodeType(
        old_code.co_argcount,
        old_code.co_kwonlyargcount,
        old_code.co_nlocals,
        old_code.co_stacksize,
        old_code.co_flags,
        new_code + old_code.co_code,
        old_code.co_consts,
        old_code.co_names,
        old_code.co_varnames,
        old_code.co_filename,
        old_code.co_name,
        old_code.co_firstlineno,
        old_code.co_lnotab,
        old_code.co_freevars,
        old_code.co_cellvars,
    )
